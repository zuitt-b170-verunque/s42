
// all encompassing selector
document.querySelector("txt-first-name");

/* 
    document - refers to the whole webpage
    querySelector - used to select a specific element (object) as long as it is inside the html tag (HTML element)
                  - takes a string that is in formatted like CSS selector
                  - can select elements regardless if the string is an id, a class or a tag; as long as it exists on the webpage
*/


/* 
// similar to queryselector but more specific
document.getElementById("txt-first-name");
document.getElementByClass("txt-inputs");
document.getElementByTagName("input"); 
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name")
const txtFullName = document.querySelector("#span-full-name");

// ACTIVITY

/* 
Copy the index.html and index.js to the activity folder.
Listen to an event when the last name's input is changed.

Instead of anonymous functions for each of the event listener:

Create a function that will update the span's contents based 

on the value of the first and last name input fields.

Instruct the event listeners to use the created function.

*/

txtLastName.addEventListener("keyup", (event) => {
    txtFullName.innerHTML =[txtFirstName.value, txtLastName.value] ;
}) 



/* function pushName () {
    let firstName = txtFirstName.value
    let lastName = txtLastName.value

    txtFullName.innerHTML = firstName && lastName
} 

txtFirsttName.addEventListener("keyup", pushName)
txtLastName.addEventListener("keyup", pushName)

*/

/* txtLastName.addEventListener("keyup", function(event) {
    document.querySelector("#span-full-name").innerHTML = event.target.value;
}); */


