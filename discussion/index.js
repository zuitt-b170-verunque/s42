
// all encompassing selector
document.querySelector("txt-first-name");

/* 
    document - refers to the whole webpage
    querySelector - used to select a specific element (object) as long as it is inside the html tag (HTML element)
                  - takes a string that is in formatted like CSS selector
                  - can select elements regardless if the string is an id, a class or a tag; as long as it exists on the webpage
*/


/* 
// similar to queryselector but more specific
document.getElementById("txt-first-name");
document.getElementByClass("txt-inputs");
document.getElementByTagName("input"); 
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", (event) => {
    txtFullName.innerHTML = txtFirstName.value + txtLastName.value;
})

/* 
    event - actions that the user is doing in our webpage ()

    addEventListener - a function that lets the webpage to listen to the events performed by the user
                     - takes two arguments
                        -String - the events which the HTML element will listen
                        - function - executed by the element once the event (first argument) is triggered

*/


// makes the element listen to multiple event
txtFirstName.addEventListener("keyup", (event) => {
    // trying to log the codes, instead of it's value
    console.log(event.target);
    //  trying to to log the value of the element once the event is triggered 
    console.log(event.target.value);
})


// mini activity

const txtLastName = document.querySelector("#txt-last-name")

txtLastName.addEventListener("keyup", (event) => {
    txtFullName.innerHTML =txtLastName.value;
})



